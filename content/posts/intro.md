---
title: "BEAR Intro"
date: 2021-08-10T22:59:32+02:00
lastMod: .LastMod.Format
draft: false
Tags: ["video", "intro", "bear"]
---

Today, I want to share with you a small intro video that I have made to both illustrate this
website's name and to have in longer videos.

Without further ado, enjoy the video ! 

{{< vimeo 585677839 >}}
