---
title: "Hello Friends!"
date: 2021-08-09T22:54:36+02:00
lastMod: .LastMod.Format
draft: false
Tags: ["video", "intro", "bear"]
---
Hello, I am Freyja, sometimes known as Bear.

Welcome to this silly website, where I will share short videos I made with bricks, and a lot of patience.

It is still a work in progress, so bear *(HAHA!)* with me for a moment.

This is the current logo of this website, it has been made with [Krita](https://krita.org/en/) and a modicum of bad taste and low graphical skill from a bear.

![logo of a LEGO minifig with a bear head holding a guitar. A rainbow is badly added to the guitar, a greyed-out explosion is on the side and an astronaut LEGO minifig sits on top of the picture](logo.png)

This website is called **Bricks, Explosions, Astronauts, and Rainbows**, because it turns into **BEAR**, but also because it is what I have for now in my little videos.

You may find here more videos, gifs, and pictures of silly Lego stuff.

Currently, my videos are hosted on [vimeo](https://vimeo.com/user147389464)

Some ramblings of a Bear might appear randomly.


---