---
title: "Who is Bear"
date: 2021-08-15T18:35:28+02:00
lastMod: .LastMod.Format
draft: false
Tags: ["video", "introduction", "bear"]
---
I'm sure you're all very curious as to who this Bear is that you've been seeing so much about.

To give you the much needed information you seek, the Beary Good Ministry of Bear Information has released this video about Bear.

You will find there that there are a few important things in life for Bear.

{{< vimeo 587497124 >}}

I hope you have enjoyed the video, and if you should remember only one thing from all this:


***Gimme all your cookies!***