---
title: "Cleaner Bear"
date: 2021-08-15T18:21:56+02:00
draft: false
Tags: ["video", "bear", "clean"]
---

Today brings a new short video showing how Bears also have to clean up and use brooms.

{{< vimeo 587494348 >}}
