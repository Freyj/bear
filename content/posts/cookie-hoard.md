---
title: "Cookie Hoard"
date: 2021-08-09T23:54:36+02:00
lastMod: .LastMod.Format
draft: false
Tags: ["video", "cookie", "bear"]
---
This bear's main food source is, obviously, cookies.

And for that, a bear requires a sufficient stash of cookies, as evidenced by the following video:

{{< vimeo 585514014 >}}
