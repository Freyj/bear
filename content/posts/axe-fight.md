---
title: "Axe Fight"
date: 2021-08-11T23:38:38+02:00
lastMod: .LastMod.Format
draft: false
Tags: ["video", "bear", "axe"]
---

Today brings a new short video showing how Bears used swords to defeat axe-wielding enemies in the medieval times.

{{< vimeo 586011470 >}}
